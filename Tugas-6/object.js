function arrayToObject(people) {
    // console.log(people.length);
    for (var i = 0; i < people.length; i++){
        var obj = {};
        var t = people[i][3];
        var y = new Date().getFullYear();
        if(!people[i][3] || t > y){
            age = "Invalid Birth Year";   
        }else{
            age = y-t ;
        }
        obj.firstName       = input[i][0]
        obj.lastName        = input[i][1]
        obj.gender          = input[i][2]
        obj.age             = age

        var output = (i+1) + ' - ' + obj.firstName + obj.lastName + ' : ';
        console.log(output);
        console.log(obj);
        console.log("\n");
    }
    //return data;    
}

var input = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"],
                ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] 
            ];
console.log("---No 1--- \n");
arrayToObject(input);

console.log("---No 2--- \n");
function shoppingTime(memberId, money){
    var brand = [["Sepatu Stacattu",1500000],
                ["Baju Zoro",500000],
                ["Baju H&N",250000],
                ["Sweater Uniklooh",175000],
                ["Casing Handphone",50000]];    
    if (!memberId){
        console.log("Mohon maaf, toko X hanya berlaku untuk member saja");
    }else if(money < 50000){
        console.log("Mohon maaf, uang tidak cukup");
    }else if(!memberId || !money){
        console.log("Mohon maaf, toko X hanya berlaku untuk member saja");
    }else{
        var obj ={};  
        var beli = [];
        var kembali = 0;
        var kembalian = 0;
        for(var i = 0; i < brand.length; i++){
                if(brand[i] == 0){
                    if(money >= brand[i][1]){
                        beli.push(brand[i][0]);
                        kembali += money - brand[i][1];
                    }else{
                        beli.push(brand[i+1][0]);
                        kembali += money - brand[i][1];
                    }
                }else{
                    if(kembali <= brand[i][1]){
                        beli.push(brand[i][0]);
                        kembali -= brand[i][1];
                    }else{
                        beli.push(brand[i+1][0]);
                        kembali -= brand[i][1];
                    }
                }
            kembalian = kembali;    
        }
            obj.memberId      = memberId;
            obj.money         = money;
            obj.listPurchased = beli;
            obj.changeMoney   = kembalian;  

            console.log(obj);
    }          
}


shoppingTime('1820RzKrnWn08', 2475000);
//console.log(shoppingTime('82Ku8Ma742', 170000));
//console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
//console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
//console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja


console.log("\n --- No 3 ---");
function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    if (!arrPenumpang){
        console.log("[]");
    }else{
        for(var i = 0; i < arrPenumpang.length; i++){
            var obj = {};
            obj.penumpang = arrPenumpang[i][0];
            obj.naikDari = arrPenumpang[i][1];
            obj.Tujuan = arrPenumpang[i][2];
                var jarak = 0;
                for (var j=0; j < rute.length; j++){
                    var count1  = 0;
                    var count2  = 0;
                    if(arrPenumpang[i][1] == rute[j]){
                        count1 = j;
                    }
                    if(arrPenumpang[i][2] == rute[j]){
                        count2 = j;
                    }
                    jarak += count2-count1;
                    
                }
            obj.harga = jarak*2000;
            console.log(obj);
        }
    } 
}

var data = [['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']];
naikAngkot(data);