//if-else 

var nama = "John";
var peran = "Guard";

if (nama == ''){
    console.log('Nama harus diisi');
}else if ((nama != '') && (peran =='')){
    console.log('Halo '+nama+', '+'Pilih peranmu untuk memulai');
}else if ((nama != '') && (peran == 'Penyihir')){
    console.log('Halo penyihir '+nama+', '+'kamu dapat melihat siapa yang menjadi werewolf!');
}else if ((nama != '') && (peran == 'Guard')){
    console.log('Halo guard '+nama+', '+'kamu akan membantu melindungi temanmu dari serangan werewolf.');
}else if ((nama != '') && (peran == 'Werewolf')){
    console.log('Halo werewolf '+nama+', '+'Kamu akan memakan mangsa setiap malam!');
} 

//switch case
var hari = 21; 
var bulan = 1; 
var tahun = 1945;
switch(bulan) {
    case 1:
        text = "Januari";
        break;
    case 2:
        text = "Febuari";
        break;
    case 3:
        text = "Maret";
        break;
    case 4:
        text = "April";
        break;
    case 5:
        text = "Mei";
        break;
    case 6:
        text = "Juni";
        break;
    case 7:
        text = "Juli!";
        break;
    case 8:
        text = "Agustus";
        break;
    case 9:
        text = "September";
        break;
    case 10:
        text = "Oktober";
        break;
    case 11:
        text = "November";
        break;
    case 12:
        text = "Desember";
        break;
} 
console.log(hari+' '+text+' '+tahun);