var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Lanjutkan code untuk menjalankan function readBooksPromise 
function baca(waktu,books,i){
    if (i < books.length){
        readBooksPromise(waktu,books[i])
        .then(function (fulfilled) {
            if (fulfilled > 0){
                i+=1
                baca(fulfilled,books,i)
            }
        })
        .catch(function (error) {
            console.log(error.message);
        });
    }
    
}

baca(10000,books,0)