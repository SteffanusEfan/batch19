// di index.js
var readBooks = require('./callback.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000},
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Tulis code untuk memanggil function readBooks di sini
function baca(waktu,books,i){
    if (i < books.length){
        readBooks(waktu,books[i], function(sisa){
            if (sisa > 0){
                i+=1;
                baca(sisa,books,i)
            }    
        })    
    }    
}
baca(10000,books,0)
// var data = [{name: 'LOTR', timeSpent: 3000}]
// readBooks(3000,data)