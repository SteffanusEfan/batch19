//No 1 looping while
var i = 2;
console.log("LOOPING PERTAMA WHILE");
while(i <= 20){
    console.log(i + ' - I love coding');
    i+=2;
}

// No 2 looping while 
var j = 20;
var k = 0;
console.log("\n LOOPING KEDUA WHILE");
while(j-2){
    console.log(k+j + ' - I love coding');
    j-=2;
}

// No 1 looping for
var a = 20;
console.log("\n LOOPING GANJIL GENAP");
for (b = 1; b <= a; b++){
    if((b % 3 == 0) && (b % 2 == 1)){
        console.log(b + ' - I Love Coding');
    }else if (b % 2 == 1){
        console.log(b + ' - Santai');    
    }else if (b % 2 == 0){
        console.log(b + ' - Berkualitas');
    }
}

// No 2 looping for
console.log("\n LOOPING PERSEGI PANJANG");
var temp = "";
for (d = 1; d <= 4; d++){
    temp = "";
    for (c = 1; c<=8; c++){
        temp += "#";
    }   
    console.log(temp);  
}

//No 3 Looping for 
console.log("\n LOOPING TANGGA");
var tempt="";
for (e=1;e<=7;e++){
  tempt+="#";  
  for (f=1;f<=7;f++){
       
   }console.log(tempt);
}

// No 4 looping for
console.log("\n LOOPING CATUR");
var tempo = "";
for (m = 1; m <= 8; m++){
    tempo = "";
    for (n = 1; n<=8; n++){
      if (m%2==1){     
        if(n%2==1){
          tempo += " ";
        }else{  
          tempo += "#";
        }
      }else{
        if(n%2==1){
          tempo += "#";
        }else{  
          tempo += " ";
        }
      }
    }   
    console.log(tempo);  
}

