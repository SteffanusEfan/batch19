console.log("Animal Class")
class Animal {
    constructor(name) {
      this._name = name;
      this._legs = 4
      this._cold_blooded = false
    }
    get name() {
      return this._name;
    }
    set aname(x) {
      this._name = x;
    }
    get legs() {
        return this._legs;
      }
    set legs(x) {
    this._legs = x;
    }
    get cold_blooded() {
        return this._cold_blooded;
      }
    set cold_blooded(x) {
    this._cold_blooded = x;
    }
  }
  console.log("Release 0")
  var sheep = new Animal("shaun");
 
  console.log(sheep.name) // "shaun"
  console.log(sheep.legs) // 4
  console.log(sheep.cold_blooded) // false

  class Ape extends Animal{
    constructor(name){
        super(name);
    }
    yell(){
        return "Auoo";
    }
  }

  class Frog extends Animal{
    constructor(name){
        super(name);
    }
    jump(){
        return "hop hop";
    }
  }
  
console.log("\n Release 1")
var sungokong = new Ape("kera sakti")
console.log(sungokong.yell()) // "Auooo"
 
var kodok = new Frog("buduk")
console.log(kodok.jump()) // "hop hop" 


console.log("\n Function to Class")
class Clock{
    constructor(template){
        this.template = template;
        this.timer =0;
    }
  
    render(){
        var date = new Date();

        var hours = date.getHours();
        if (hours < 10) hours = '0' + hours;

        var mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;

        var secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;

        var output = template
        .replace('h', hours)
        .replace('m', mins)
        .replace('s', secs);

        console.log(output);
    }
    start(){
        clearInterval(this.timer);
    }
    stop(){
        this.render();
        this.timer = setInterval(()=>this.render(), 1000);
    }
}
var clock = new Clock({template: 'h:m:s'});
console.log(clock.start()); 

// function Clock({ template }) {
  
//     var timer;
  
//     function render() {
//       var date = new Date();
  
//       var hours = date.getHours();
//       if (hours < 10) hours = '0' + hours;
  
//       var mins = date.getMinutes();
//       if (mins < 10) mins = '0' + mins;
  
//       var secs = date.getSeconds();
//       if (secs < 10) secs = '0' + secs;
  
//       var output = template
//         .replace('h', hours)
//         .replace('m', mins)
//         .replace('s', secs);
  
//       console.log(output);
//     }
  
//     this.stop = function() {
//       clearInterval(timer);
//     };
  
//     this.start = function() {
//       render();
//       timer = setInterval(render, 1000);
//     };
  
//   }
  
//   var clock = new Clock({template: 'h:m:s'});
//   clock.start(); 