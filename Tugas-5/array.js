function range(startNum, finishNum){
    var temp = [];
    if(startNum > finishNum){
        var a = startNum - finishNum +1;
        for (var i = 0; i < a; i++){
            temp.push(startNum - i);
        }
    }else if (startNum < finishNum){
        var a = finishNum - startNum +1;
        for(var i = 0; i < a; i++){
            temp.push(startNum+i);
        }
    }else if (!startNum || !finishNum){
        return -1;
    }
    return temp;
}
console.log("Soal No 1");
console.log(range(1, 10)); 
console.log(range(1)) ;
console.log(range(11,18)); 
console.log(range(54, 50));
console.log(range());  

function rangeWithStep(startNum, finishNum, step) {
    var temp = [];
    if(startNum > finishNum){
        var a = startNum;
        for (var i = 0; a >= finishNum; i++){
            temp.push(a);
            a -= step;
        }
    }else if (startNum < finishNum){
        var a = startNum;
        for(var i = 0; a <= finishNum; i++){
            temp.push(a);
            a += step;
        }
    }else if (!startNum || !finishNum || !step){
        return -1;
    }
    return temp;
}
console.log("\n Soal no 2")
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 


function sum(startNum, finishNum, step){
    var total = 0;
    if(!startNum && !finishNum && !step){
        return  0;
    }else if(!startNum || !finishNum || !step){
        return 1;
    }else if (!step ){
        step = 1;
        temp = rangeWithStep(startNum, finishNum, step);
        total = temp.reduce((a,b)=>a+b,0);
    }else{
        temp = rangeWithStep(startNum, finishNum, step);
        total = temp.reduce((a,b)=>a+b,0);
    } 
    return total;
}

console.log("\n Soal no 3")
console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 

function dataHandling(input){
    var tampung = "";
    for (var i=0; i < input.length; i++){
        var tempo = input[i];
        //console.log(tempo.length);
        for(var j = 0; j < tempo.length; j++){
            if (j == 0){
                tampung += "Nomor ID :" + input[i][j] + "\n";
             }else if (j == 1){
                tampung += "Nama Lengkap :" + input[i][j] + "\n";
            }else if (j == 2){
                tampung += "TTL :" + input[i][j] + "\n";
            }else if (j == 3){
                tampung += "Hobi :" + input[i][j] + "\n";
            }
        }
    }
    return tampung;
}

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
];
console.log("No 4");
console.log(dataHandling(input));

function balikKata(kata){
    var split = kata.split('');
    var balik = split.reverse();
    var join = balik.join('');
    return join;
}
console.log("\n No 5");
console.log(balikKata("Kasur Rusak")); // kasuR rusaK
console.log(balikKata("SanberCode")); // edoCrebnaS
console.log(balikKata("Haji Ijah")); // hajI ijaH
console.log(balikKata("racecar")); // racecar
console.log(balikKata("I am Sanbers")); // srebnaS ma I 

function dataHandling2(dataku){
    var nama = dataku.splice(1,1,"Roman Alamsyah Elsharawy");
    console.log(nama);
    
    console.log("Mei");
    
    var d = dataku[3].split("/");
    var d0 = d[0];
    var d1 = d[1];
    var d2 = d[2]; 
    
    var joind2 = d.splice(1,0,d[2]);
    console.log(joind2);
    var joind = d[1]+"-"+d[0]+"-"+d[2];
    console.log(joind);

    var e = dataku[1];
    console.log(e);
}
var dataku = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
console.log("\n No 6");
dataHandling2(dataku);

